#!/bin/sh

_BASE_URL="https://ezezaguna.poo.li"
_DESTINATION="public"

wget 'https://ezezaguna.poo.li/sitemap.xml' -O sitemap.xml || exit 1

_PAGES="$(grep -o -e '<loc>https://ezezaguna.poo.li[a-zA-Z0-9/\_-]*</loc>' sitemap.xml | sed -e 's%\(<loc>\)\|\(</loc>\)%%g' -e 's%https://ezezaguna.poo.li%%g')"

for page in $_PAGES ; do
    if [ -e "${_DESTINATION}${page}index.html" ]; then
        echo "${_DESTINATION}${page}index.html already exists"
        continue
    fi
    echo "ezezaguna.gitlab.io${page} -> ${_BASE_URL}${page}"
    mkdir -p "${_DESTINATION}${page}"
    sed -e "s%@URL@%${_BASE_URL}${page}%g" redirect-template.html > "${_DESTINATION}${page}index.html"
done

